﻿using System;
using System.Collections.Generic;
using System.Linq;
using LibGit2Sharp;

namespace FileCommitDetector
{
    class Program
    {
        public static void Main(string[] args)
        {
            string path = args.Length > 0 ? args[0] : AppDomain.CurrentDomain.BaseDirectory + "../../../";
            string stopCommmit = args.Length > 1 ? args[1] : string.Empty;
            using (var repo = new Repository(path))
            {
                Dictionary<string, int> touches = new Dictionary<string, int>();
                Console.WriteLine($"Repo: {repo.Info.WorkingDirectory} - {repo.Head.FriendlyName}");

                var filter = new CommitFilter()
                {
                    SortBy = CommitSortStrategies.None
                };
                IEnumerable<Commit> commits = repo.Commits.QueryBy(filter);
                Tree comparingTree = repo.Head.Tip.Tree;
                foreach (var commit in commits)
                {
                    IterateCommit(repo, touches, string.Empty, commit.Tree, comparingTree);
                    if (commit.Sha == stopCommmit)
                    {
                        Console.WriteLine($"Commit {stopCommmit.Substring(0, 6)} {commit.Committer.When} '{commit.MessageShort}' reached. Finish");
                        break;
                    }
                    comparingTree = commit.Tree;
                }
                PrintResult(touches, commits);
            }

        }
        private static void PrintResult(Dictionary<string, int> touches, IEnumerable<Commit> commits)
        {
            Console.WriteLine($"==== {commits.Count()} commits analyzed");
            foreach (var item in touches.Where(x => x.Value > 1).OrderByDescending(x => x.Value).ThenBy(x => x.Key))
                Console.WriteLine($"{item.Value,-15}{item.Key}");
        }
        private static void IterateCommit(IRepository repo, Dictionary<string, int> touches, string prefix, Tree tree, Tree parentTree)
        {
            var patch = repo.Diff.Compare<Patch>(parentTree, tree);
            foreach (var ptc in patch)
            {
                if (!touches.ContainsKey(ptc.Path))
                    touches[ptc.Path] = 1;
                else
                    touches[ptc.Path]++;
            }
        }
    }
}